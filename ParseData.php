<?php
/*ParseData.php
A class for extracting default values and performing empty checks on php variables
Copyright (C) 2016,2017 Daniel Fredriksen
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

namespace CYINT\ComponentsPHP\Classes;
class ParseData
{

    public static function setArray($value, $key, $default = null, $boolean = false)
    {       
        $result = (empty($value[$key])) ? $default : $value[$key];
        if($boolean)
            return empty($result) ? false : true;
        else
        return $result;
    }

    public static function set($value, $default = null)
    {
        return empty($value) ? $default : $value;
    }

    public static function setFromMethod($object, $method, $default = null)
    {
        if(empty($object) || !method_exists($object, $method)) return $default;
        return $object->$method();
    }

    public static function createDataArray($value, $keys)
    {
        $fields = [];
        if(count($keys) > 0) 
        {
            foreach($keys as $key=>$field)
            {
                $fields[$field] = self::setArray($value, $key, null);
            }
        }

        return $fields;
    }

    public static function setIfTrue($condition, $callback, $default = null)
    {
        if($condition) return $callback();
        return $default;
    }
}

?>
